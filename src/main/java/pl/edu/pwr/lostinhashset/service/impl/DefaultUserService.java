package pl.edu.pwr.lostinhashset.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.edu.pwr.lostinhashset.entity.User;
import pl.edu.pwr.lostinhashset.repository.UserRepository;
import pl.edu.pwr.lostinhashset.service.UserService;

public class DefaultUserService implements UserService {
		
	private UserRepository userRepository;
	
	@Override
	public List<User> getUsersSortedByName() {
		
		List<User> users = getUserRepository().getUsers();
		Collections.sort(users);
		return users;
	}

	@Override
	public List<User> getUsersSortedByPoints() {

		List<User> users = getUserRepository().getUsers();
		Collections.sort(users, new Comparator<User>()
				{
			public int compare(User u1, User u2)
			{
				return u1.getPoints() < u2.getPoints() ? -1 : u1.getPoints() == u2.getPoints() ? 0 : 1;
			}
				});
		Collections.sort(users, (User u1, User u2) -> u1.getPoints() < u2.getPoints() ? -1 : u1.getPoints() == u2.getPoints() ? 0 :1);
		return users;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

}
