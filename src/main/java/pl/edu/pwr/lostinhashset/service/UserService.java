package pl.edu.pwr.lostinhashset.service;

import java.util.List;

import pl.edu.pwr.lostinhashset.entity.User;

public interface UserService {

	List<User> getUsersSortedByName();

	List<User> getUsersSortedByPoints();
	
}
