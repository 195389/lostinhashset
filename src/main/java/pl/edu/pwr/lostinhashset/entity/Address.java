package pl.edu.pwr.lostinhashset.entity;

public class Address {

	private static final char SPACE = ' ';
	private static final Character COMMA = ',';
	private static final String SLASH = "/";
		
	private String street;
	private String homeNo;
	private String flatNo;
	private String zipCode;
	private String town;
	
	
	
	@Override
	public String toString() {
		String address = "";
		if (street != null && !street.isEmpty()) {
			address += street;
			address += SPACE;
		}
		if (homeNo != null && !homeNo.isEmpty()) {
			address += homeNo;
			if (flatNo != null && !flatNo.isEmpty()) {
				address += SLASH;
				address += flatNo;
			}
			address += COMMA;
			address += SPACE;
		}
		if (zipCode != null && !zipCode.isEmpty()) {
			address += zipCode;
			address += SPACE;
		}
		if (town != null && !town.isEmpty()) {
			address += town;
		}
		
		return address;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setHomeNo(String homeNo) {
		this.homeNo = homeNo;
	}

	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getStreet() {
		return street;
	}

	public String getHomeNo() {
		return homeNo;
	}

	public String getFlatNo() {
		return flatNo;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getTown() {
		return town;
	}
	
}
