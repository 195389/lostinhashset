package pl.edu.pwr.lostinhashset.entity;

import java.util.Collections;

public class User implements Comparable<User> {

	public interface Comparable<User> { 
	     int compareTo(User obj);
	  }
	
	public interface Comparator<User> {
		int compare(User obj1, User obj2);
	}
	
	@Override
	public int hashCode() {
		//final int prime = 31;
		int prime = (int)(100*Math.random());
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + points;
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (points != other.points)
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
	private String name;
	private String surname;
	private int points;
	
	public User() {
		
	}
	
	public User(String name, String surname, int points) {
		this.name = name;
		this.surname = surname;
		this.points = points;
	}
	
	
	@Override
	public int compareTo(User u)
	{
		int nameDiff = this.getName().compareTo(u.getName());
		
		if(nameDiff !=0) { return nameDiff;}
		
		int pointsDiff = this.getPoints() - u.getPoints();
		
		return pointsDiff;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	
	
}
