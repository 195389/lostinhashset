package pl.edu.pwr.lostinhashset.entity;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class UserTest {
	
	private Set<User> registeredUsers;
	private User sampleUser;
	
	@Test
	public void shouldLooseUserInHashset() {
		//given
		registeredUsers = new HashSet<User>();
		sampleUser = new User();
		
		//when
		registeredUsers.add(sampleUser);
		
		//then
		assertFalse(registeredUsers.contains(sampleUser));
		
	}

}
